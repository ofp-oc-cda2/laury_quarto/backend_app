<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ApiResource]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Utilisateur::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $de_utilisateur;

    #[ORM\ManyToOne(targetEntity: Utilisateur::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $a_utiliseur;

    #[ORM\Column(type: 'text')]
    private $message;

    #[ORM\ManyToOne(targetEntity: Discussion::class, inversedBy: 'messages')]
    #[ORM\JoinColumn(nullable: false)]
    private $discussion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeUtilisateur(): ?Utilisateur
    {
        return $this->de_utilisateur;
    }

    public function setDeUtilisateur(?Utilisateur $de_utilisateur): self
    {
        $this->de_utilisateur = $de_utilisateur;

        return $this;
    }

    public function getAUtiliseur(): ?Utilisateur
    {
        return $this->a_utiliseur;
    }

    public function setAUtiliseur(?Utilisateur $a_utiliseur): self
    {
        $this->a_utiliseur = $a_utiliseur;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getDiscussion(): ?Discussion
    {
        return $this->discussion;
    }

    public function setDiscussion(?Discussion $discussion): self
    {
        $this->discussion = $discussion;

        return $this;
    }
}
