<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AbonnementRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AbonnementRepository::class)]
#[ApiResource]
class Abonnement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Utilisateur::class, inversedBy: 'abonnements')]
    private $utilisateur;

    #[ORM\ManyToOne(targetEntity: InfoVendeur::class, inversedBy: 'abonnements')]
    private $infoVendeur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getInfoVendeur(): ?InfoVendeur
    {
        return $this->infoVendeur;
    }

    public function setInfoVendeur(?InfoVendeur $infoVendeur): self
    {
        $this->infoVendeur = $infoVendeur;

        return $this;
    }
}
