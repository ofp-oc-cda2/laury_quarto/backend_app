<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\InfoVendeurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InfoVendeurRepository::class)]
#[ApiResource]
class InfoVendeur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToOne(targetEntity: Utilisateur::class, cascade: ['persist', 'remove'])]
    private $utilisateur;

    #[ORM\Column(type: 'integer')]
    private $nombreVente;

    #[ORM\Column(type: 'boolean')]
    private $profilPublic;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description;

    #[ORM\OneToMany(mappedBy: 'vendeur', targetEntity: CommentaireVendeur::class)]
    private $commentaireVendeurs;

    #[ORM\OneToMany(mappedBy: 'vendeur', targetEntity: Produit::class)]
    private $produits;

    #[ORM\OneToMany(mappedBy: 'infoVendeur', targetEntity: Abonnement::class)]
    private $abonnements;

    public function __construct()
    {
        $this->commentaireVendeurs = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->abonnements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getNombreVente(): ?int
    {
        return $this->nombreVente;
    }

    public function setNombreVente(int $nombreVente): self
    {
        $this->nombreVente = $nombreVente;

        return $this;
    }

    public function getProfilPublic(): ?bool
    {
        return $this->profilPublic;
    }

    public function setProfilPublic(bool $profilPublic): self
    {
        $this->profilPublic = $profilPublic;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, CommentaireVendeur>
     */
    public function getCommentaireVendeurs(): Collection
    {
        return $this->commentaireVendeurs;
    }

    public function addCommentaireVendeur(CommentaireVendeur $commentaireVendeur): self
    {
        if (!$this->commentaireVendeurs->contains($commentaireVendeur)) {
            $this->commentaireVendeurs[] = $commentaireVendeur;
            $commentaireVendeur->setVendeur($this);
        }

        return $this;
    }

    public function removeCommentaireVendeur(CommentaireVendeur $commentaireVendeur): self
    {
        if ($this->commentaireVendeurs->removeElement($commentaireVendeur)) {
            // set the owning side to null (unless already changed)
            if ($commentaireVendeur->getVendeur() === $this) {
                $commentaireVendeur->setVendeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Produit>
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addProduit(Produit $produit): self
    {
        if (!$this->produits->contains($produit)) {
            $this->produits[] = $produit;
            $produit->setVendeur($this);
        }

        return $this;
    }

    public function removeProduit(Produit $produit): self
    {
        if ($this->produits->removeElement($produit)) {
            // set the owning side to null (unless already changed)
            if ($produit->getVendeur() === $this) {
                $produit->setVendeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Abonnement>
     */
    public function getAbonnements(): Collection
    {
        return $this->abonnements;
    }

    public function addAbonnement(Abonnement $abonnement): self
    {
        if (!$this->abonnements->contains($abonnement)) {
            $this->abonnements[] = $abonnement;
            $abonnement->setInfoVendeur($this);
        }

        return $this;
    }

    public function removeAbonnement(Abonnement $abonnement): self
    {
        if ($this->abonnements->removeElement($abonnement)) {
            // set the owning side to null (unless already changed)
            if ($abonnement->getInfoVendeur() === $this) {
                $abonnement->setInfoVendeur(null);
            }
        }

        return $this;
    }
}
