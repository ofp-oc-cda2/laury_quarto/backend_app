<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProduitRepository::class)]
#[ApiResource]
class Produit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Categorie::class, inversedBy: 'produits')]
    private $categorie;

    #[ORM\ManyToOne(targetEntity: InfoVendeur::class, inversedBy: 'produits')]
    private $vendeur;

    #[ORM\Column(type: 'string', length: 255)]
    private $titre;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'integer')]
    private $prix;

    #[ORM\Column(type: 'boolean')]
    private $afficher;

    #[ORM\Column(type: 'integer')]
    private $quantite;

    #[ORM\Column(type: 'datetime')]
    private $publication;

    #[ORM\OneToMany(mappedBy: 'produit', targetEntity: CommentaireProduit::class)]
    private $commentaireProduits;

    #[ORM\OneToMany(mappedBy: 'produit', targetEntity: CommandeDetails::class)]
    private $commandeDetails;

    #[ORM\OneToMany(mappedBy: 'produit', targetEntity: ImageProduit::class)]
    private $imageProduits;

    public function __construct()
    {
        $this->commentaireProduits = new ArrayCollection();
        $this->commandeDetails = new ArrayCollection();
        $this->imageProduits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getVendeur(): ?InfoVendeur
    {
        return $this->vendeur;
    }

    public function setVendeur(?InfoVendeur $vendeur): self
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getAfficher(): ?bool
    {
        return $this->afficher;
    }

    public function setAfficher(bool $afficher): self
    {
        $this->afficher = $afficher;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getPublication(): ?\DateTimeInterface
    {
        return $this->publication;
    }

    public function setPublication(\DateTimeInterface $publication): self
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * @return Collection<int, CommentaireProduit>
     */
    public function getCommentaireProduits(): Collection
    {
        return $this->commentaireProduits;
    }

    public function addCommentaireProduit(CommentaireProduit $commentaireProduit): self
    {
        if (!$this->commentaireProduits->contains($commentaireProduit)) {
            $this->commentaireProduits[] = $commentaireProduit;
            $commentaireProduit->setProduit($this);
        }

        return $this;
    }

    public function removeCommentaireProduit(CommentaireProduit $commentaireProduit): self
    {
        if ($this->commentaireProduits->removeElement($commentaireProduit)) {
            // set the owning side to null (unless already changed)
            if ($commentaireProduit->getProduit() === $this) {
                $commentaireProduit->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CommandeDetails>
     */
    public function getCommandeDetails(): Collection
    {
        return $this->commandeDetails;
    }

    public function addCommandeDetail(CommandeDetails $commandeDetail): self
    {
        if (!$this->commandeDetails->contains($commandeDetail)) {
            $this->commandeDetails[] = $commandeDetail;
            $commandeDetail->setProduit($this);
        }

        return $this;
    }

    public function removeCommandeDetail(CommandeDetails $commandeDetail): self
    {
        if ($this->commandeDetails->removeElement($commandeDetail)) {
            // set the owning side to null (unless already changed)
            if ($commandeDetail->getProduit() === $this) {
                $commandeDetail->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ImageProduit>
     */
    public function getImageProduits(): Collection
    {
        return $this->imageProduits;
    }

    public function addImageProduit(ImageProduit $imageProduit): self
    {
        if (!$this->imageProduits->contains($imageProduit)) {
            $this->imageProduits[] = $imageProduit;
            $imageProduit->setProduit($this);
        }

        return $this;
    }

    public function removeImageProduit(ImageProduit $imageProduit): self
    {
        if ($this->imageProduits->removeElement($imageProduit)) {
            // set the owning side to null (unless already changed)
            if ($imageProduit->getProduit() === $this) {
                $imageProduit->setProduit(null);
            }
        }

        return $this;
    }
}
