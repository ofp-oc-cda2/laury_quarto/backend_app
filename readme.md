# backend projet CDA (en cours => branche 'dev')

## 1. Description
Repo du backend de l'API REST
Création des différentes entitées (voir diagramme de classes https://gitlab.com/ofp-oc-cda2/laury_quarto/conception_projet_cda) et leurs relations.


## 2. Techno utilisées :
* Symfony 6
* APIPlatform

## 3. Installation en local :
Utiliser le docker présent pour faire tourner l'application (back + front).

